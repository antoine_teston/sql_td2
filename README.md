# sql_td2

-------------------------------------------------
### Data Management
-------------------------------------------------

### Objectifs du TD de SQL n°2

10/2022 / M1 Health Data Science

- Développement création/suppression table en script SQL

- Utilisation de l'outil Dbeaver

- Alimentation de données, enregistrement et exportation de
table en fichier.csv

- Objectif de sortie de données en exportation .csv après alimentation manuelle
de données

### En exemple :

Script SQL

	CREATE TABLE RUM (
	ID_RUM         INT PRIMARY KEY NOT NULL,
	ID_SEJOUR      VARCHAR(10) 	NOT NULL,
	DATE_ENTREE    DATE 			NOT NULL,
	DATE_SORTIE    DATE 			NOT NULL,
	MODE_ADMISSION VARCHAR(20) 	NOT NULL,
	MODE_SORTIE    VARCHAR(20) 	NOT NULL 
	-- id rum.rum to id rum.rumacte
	-- id rum.rum to id rum.rumdiagnost
	--)

Avec insertion des données : 

	INSERT INTO rum  (
	id_rum, id_sejour, date_entree, date_sortie, mode_admission, mode_sortie)
	VALUES 
	(1, '00000100', '01-05-2020', '01-06-2020', 'URG', 'RAD'),
	(2, '00000101', '01-06-2020', '01-07-2020', 'PROG', 'RAD'),
	(3, '00000102', '01-07-2020', '01-08-2020', 'PROG', 'RAD'),
	(4, '00000103', '01-08-2020', '01-09-2020', 'URG', 'RAD'),
	(5, '00000104', '01-09-2020', '01-10-2020', 'URG', 'RAD')


Et en sortie rum_sqlTD2.csv: 

|id_rum|id_sejour|date_entree|date_sortie|mode_admission|mode_sortie|
|------|------|------|------|------|------|
|1|00000100 | 2020-01-05| 2020-01-06|URG           |RAD        |
|2|00000101 | 2020-01-06| 2020-01-07|PROG          |RAD        |
|3|00000102 | 2020-01-07| 2020-01-08|PROG          |RAD        |
|4|00000103 | 2020-01-08| 2020-01-09|URG           |RAD        |
|5|00000104 | 2020-01-09| 2020-01-10|URG           |RAD        |