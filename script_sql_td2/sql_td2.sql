-------------------------------------------------
-- Objectifs
-------------------------------------------------

-- sql_td2 10/2022 / M1 DSS Data Management
-- Développement création/suppression table
-- Alimentation, enregistrement et exportation .csv

-------------------------------------------------
-- SESSION INFO
-------------------------------------------------
-- Dbeaver Version 22.2.2.202210092258
-- VSCode Version : 1.72.0
-- macOS Monterey/WIN 11
-- git https://gitlab.com/antoine_teston/sql_td2

-------------------------------------------------
-- Création des tables 
-------------------------------------------------

-- ville

DROP TABLE IF EXISTS ville;

CREATE TABLE ville (
	id_ville 		int NOT NULL,
	ville 			varchar(50) NULL,
	CONSTRAINT pk_ville PRIMARY KEY (id_ville)
);

INSERT INTO "antoine.teston.etu".ville (id_ville,ville) VALUES
	 (1,'Lille'),
	 (2,'Roubaix'),
	 (3,'Tourcoing'),
	 (4,'Villeneuve d’Ascq'),
	 (5,'Ronchin');

	
-- patient


DROP TABLE IF EXISTS patient;

CREATE TABLE patient (
	id_patient 		int NOT NULL,
	date_naissance	date NULL,
	sexe			char(1) null,
	id_ville		integer not null,
	CONSTRAINT pk_patient PRIMARY KEY (id_patient),
	constraint fk_patient_ville foreign key (id_ville) references ville (id_ville)
); 

INSERT INTO "antoine.teston.etu".patient (id_patient,date_naissance,sexe,id_ville) VALUES
	 (1,'1997-05-21','F',2),
	 (2,'1987-02-04','F',1),
	 (3,'1956-11-28','F',2),
	 (4,'1967-12-05','F',3),
	 (5,'2001-12-28','M',1),
	 (6,'1950-12-20','F',1),
	 (7,'1977-11-10','M',1),
	 (8,'1999-08-21','M',2),
	 (9,'2005-07-23','M',4),
	 (10,'1990-01-02','F',5);
INSERT INTO "antoine.teston.etu".patient (id_patient,date_naissance,sexe,id_ville) VALUES
	 (11,'1978-05-29','M',1),
	 (12,'1991-12-15','F',1),
	 (13,'1979-05-08','M',1),
	 (14,'1982-10-09','F',1),
	 (15,'1987-10-04','M',2),
	 (16,'1995-11-19','M',3),
	 (17,'1998-12-13','F',4),
	 (18,'2010-01-09','F',5),
	 (19,'2005-12-17','F',3),
	 (20,'1940-06-21','F',2);
INSERT INTO "antoine.teston.etu".patient (id_patient,date_naissance,sexe,id_ville) VALUES
	 (21,'2000-10-10','F',3),
	 (22,'1987-09-18','M',1),
	 (23,'1971-12-30','M',2),
	 (24,'1967-11-07','M',3),
	 (25,'1965-02-20','M',4),
	 (26,'1960-03-21','F',5),
	 (27,'1980-07-18','F',3),
	 (28,'1987-12-26','M',2),
	 (29,'1956-03-25','M',1),
	 (30,'1980-09-08','F',1);
INSERT INTO "antoine.teston.etu".patient (id_patient,date_naissance,sexe,id_ville) VALUES
	 (31,'1999-12-28','F',4),
	 (32,'1991-02-07','F',1),
	 (33,'2000-01-12','M',1),
	 (34,'1987-03-20','M',1),
	 (35,'1976-02-01','M',1),
	 (36,'1965-10-10','F',1),
	 (37,'1988-12-09','M',1),
	 (38,'1930-01-08','M',2),
	 (39,'1973-10-21','F',2),
	 (40,'1981-02-20','M',3);
INSERT INTO "antoine.teston.etu".patient (id_patient,date_naissance,sexe,id_ville) VALUES
	 (41,'1966-02-17','F',3),
	 (42,'1990-09-10','F',2),
	 (43,'1945-02-03','F',1),
	 (44,'1952-02-04','M',4),
	 (45,'1940-03-02','F',5),
	 (46,'1954-10-12','M',5),
	 (47,'1952-01-04','M',1),
	 (48,'1965-10-18','F',1),
	 (49,'1961-12-30','F',1),
	 (50,'1930-12-20','M',1);

-- hopital

DROP TABLE IF EXISTS hopital;

CREATE TABLE hopital (
	id_hopital 			int NOT NULL,
	hopital 			varchar(50) NULL,
	CONSTRAINT pk_hopital PRIMARY KEY (id_hopital)
);

INSERT INTO "antoine.teston.etu".hopital (id_hopital,hopital) VALUES
	 (1,'CHU Lille'),
	 (2,'CH Roubaix'),
	 (3,'CH Tourcoing'),
	 (4,'CH Armentière');

-- sejour

DROP TABLE IF EXISTS sejour;

CREATE TABLE sejour (
	id_sejour 		int NOT NULL,
	id_patient		int not NULL,
	date_debut_sejour 	date not null,
	date_fin_sejour		date not null,
	id_hopital			int not null,
	CONSTRAINT pk_sejour PRIMARY KEY (id_sejour),
	constraint fk_sejour_patient foreign key (id_sejour) references sejour (id_sejour),
	constraint fk_sejour_hopital foreign key (id_hopital) references hopital (id_hopital),
	constraint fk_id_patient foreign key (id_patient) references patient (id_patient)
);

INSERT INTO "antoine.teston.etu".sejour (id_sejour,id_patient,date_debut_sejour,date_fin_sejour,id_hopital) VALUES
	 (1,1,'2020-01-01','2020-01-10',1),
	 (2,2,'2020-01-01','2020-01-12',1),
	 (3,2,'2020-01-20','2020-01-25',1),
	 (4,3,'2020-01-01','2020-01-08',1),
	 (5,3,'2020-02-02','2020-02-04',1),
	 (6,3,'2020-03-01','2020-03-04',1),
	 (7,4,'2020-01-02','2020-01-03',1),
	 (8,5,'2020-01-02','2020-01-02',1),
	 (9,6,'2020-01-02','2020-01-10',1),
	 (10,7,'2020-01-02','2020-01-04',1);
INSERT INTO "antoine.teston.etu".sejour (id_sejour,id_patient,date_debut_sejour,date_fin_sejour,id_hopital) VALUES
	 (11,7,'2020-01-10','2020-01-20',2),
	 (12,8,'2020-01-03','2020-01-05',2),
	 (13,9,'2020-02-05','2020-02-10',1),
	 (14,10,'2020-01-03','2020-01-03',1),
	 (15,11,'2020-01-04','2020-01-05',1),
	 (16,12,'2020-01-06','2020-01-07',1),
	 (17,13,'2020-01-06','2020-01-08',2),
	 (18,14,'2020-01-06','2020-01-08',2),
	 (19,14,'2020-02-20','2020-02-21',2),
	 (20,15,'2020-01-06','2020-01-08',2);
INSERT INTO "antoine.teston.etu".sejour (id_sejour,id_patient,date_debut_sejour,date_fin_sejour,id_hopital) VALUES
	 (21,16,'2020-01-06','2020-01-08',3),
	 (22,17,'2020-01-06','2020-01-08',3),
	 (23,18,'2020-01-06','2020-01-09',3),
	 (24,19,'2020-01-06','2020-01-09',3),
	 (25,20,'2020-01-07','2020-01-09',1),
	 (26,21,'2020-01-07','2020-01-09',2),
	 (27,22,'2020-01-17','2020-01-19',3),
	 (28,22,'2020-02-07','2020-02-19',1),
	 (29,23,'2020-01-17','2020-01-19',2),
	 (30,24,'2020-01-17','2020-01-20',3);
INSERT INTO "antoine.teston.etu".sejour (id_sejour,id_patient,date_debut_sejour,date_fin_sejour,id_hopital) VALUES
	 (31,25,'2020-01-17','2020-01-20',3),
	 (32,26,'2020-01-18','2020-01-20',1),
	 (33,27,'2020-01-18','2020-01-22',1),
	 (34,28,'2020-01-30','2020-01-31',1),
	 (35,28,'2020-02-05','2020-02-06',1),
	 (36,29,'2020-01-30','2020-01-31',1),
	 (37,30,'2020-01-30','2020-01-31',1),
	 (38,30,'2020-02-05','2020-02-06',1),
	 (39,30,'2020-04-03','2020-04-20',1),
	 (40,30,'2020-05-03','2020-05-10',1);

-- unite
-- Créer une table unité avec les colonnes suivantes :
-- id_unite
-- code_unite
-- nom_unite
-- date_debut_validite
-- date_fin_validite
-- Créer les contraintes nécessaires (clé primaire, clé étrangère) 

DROP TABLE IF EXISTS unite;

CREATE TABLE unite (
	id_unite 			int NOT NULL,
	code_unite			char(4) not null,
	nom_unite 			varchar(50) not NULL,
	date_debut_validite	date not null,
	date_fin_validite	date not null,
	CONSTRAINT pk_unite PRIMARY KEY (id_unite)
);

-- rum
-- Créer une table RUM avec les colonnes suivantes :
-- id_rum 
-- id_sejour
-- date_entree
-- date_sortie
-- mode_entree
-- mode_sortie
-- Créer les contraintes nécessaires (clé primaire, clé étrangère) 

DROP TABLE IF EXISTS rum;

CREATE TABLE rum (
	id_rum 			int NOT NULL,
	id_sejour		int not null,
	date_entree     date not NULL,
	date_sortie		date not null,
	mode_admission		char(2),
	mode_sortie		char(2),
	CONSTRAINT pk_rum PRIMARY KEY (id_rum),
	constraint fk_rum_sejour foreign key (id_sejour) references sejour (id_sejour)
);

-- acte
-- Créer une table acte avec les colonnes suivantes :
-- id_acte 
-- code_acte
-- libelle_acte
-- date_debut_validite
-- date_fin_validite

DROP TABLE IF EXISTS acte;

CREATE TABLE acte (
	id_acte 			int NOT NULL,
	code_acte			char(7) not null,
	libelle_acte 			varchar(50) not NULL,
	date_debut_validite	date not null,
	date_fin_validite	date not null,
	CONSTRAINT pk_acte PRIMARY KEY (id_acte)
);

-- acte
-- Créer une table rum_acte avec les colonnes suivantes :
-- id_acte 
-- id_rum
-- date_acte

DROP TABLE IF EXISTS rum_acte;

CREATE TABLE "antoine.teston.etu".rum_acte (
	id_acte 			int NOT NULL,
	id_rum				int not null,
	date_acte 			date not null,
	CONSTRAINT pk_rum_acte PRIMARY KEY (id_acte, id_rum, date_acte),
	constraint fk_rum_acte_rum foreign key (id_rum) references rum (id_rum),
	constraint fk_rum_acte_acte foreign key (id_acte) references acte (id_acte)
);

-- diagnostic

DROP TABLE IF EXISTS diagnostic;

CREATE TABLE diagnostic (
	id_diagnostic			int NOT NULL,
	code_diagnostic			char(4) not null,
	libelle_diagnostic 		varchar(50) not NULL,
	date_debut_validite	date not null,
	date_fin_validite	date not null,
	CONSTRAINT pk_diagnostic PRIMARY KEY (id_diagnostic)
);

-- Rum Diagnostic
DROP TABLE IF EXISTS rum_diagnostic;

CREATE TABLE rum_diagnostic (
	id_diagnostic 			int NOT NULL,
	id_rum				int not null,
	date_diagnostic 			date not null,
	CONSTRAINT pk_rum_diagnostic PRIMARY KEY (id_diagnostic, id_rum, date_diagnostic),
	constraint fk_rum_diagnostic_rum foreign key (id_rum) references rum (id_rum),
	constraint fk_rum_diagnostic_diagnostic foreign key (id_diagnostic) references diagnostic (id_diagnostic)
);


-- Suppression des tables

DROP TABLE IF EXISTS rum_acte;
DROP TABLE IF EXISTS acte;
DROP TABLE IF EXISTS rum_diagnostic;
DROP TABLE IF EXISTS diagnostic;
DROP TABLE IF EXISTS rum;
DROP TABLE IF EXISTS unite;

-- Insertion d'enregistrements

------------------- rum --------------------------

insert into rum (id_rum, id_sejour, date_entree, date_sortie, mode_admission, mode_sortie)
values
(1, 1, '2020-01-01', '2020-01-05', '5', '5'),
(2, 1, '2020-01-05', '2020-01-10', '5', '8'),
(3, 2, '2020-01-01', '2020-01-02', '8','8'),
(4, 2, '2020-01-02', '2020-01-04', '8','8'),
(5, 2, '2020-01-04', '2020-01-12', '8','8');

----------------- rum acte -----------------------

INSERT INTO rum_acte (id_rum, id_acte, date_acte)
 VALUES
 (1, 1, '2020-01-01'),
 (2, 1, '2020-01-05'),
 (3, 2, '2020-01-01'),
 (4, 3, '2020-01-04'),
 (5, 2, '2020-01-10');

------------------ rum diagnostic -------------------

INSERT INTO rum_diagnostic (id_rum, id_diagnostic, date_diagnostic)
 VALUES
 (1, 1, '2020-01-01'),
 (2, 1, '2020-01-05'),
 (3, 1, '2020-01-01'),
 (4, 4, '2020-01-02'),
 (4, 5, '2020-01-04');
-------------------------------------------------
-- Enregistrement données/exportation .csv rum, rum_acte, rum_diag
-------------------------------------------------
-- Exportation à l'aide de l'outil d'extraction de Dbeaver
-- Fichiers CSV ajoutés au dossier d'export avant push Git


-------------------------------------------------
-------------------------------------------------

